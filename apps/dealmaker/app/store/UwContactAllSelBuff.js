Ext.define('DealMaker.store.UwContactAllSelBuff', {
    extend: 'Ext.data.BufferedStore',

    requires: [
        'DealMaker.model.Contact',
        'Ext.data.proxy.Rest',
        'Ext.data.reader.Json',
        'Ext.util.Sorter'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            pageSize: 50,
            storeId: 'UwContactAllSelBuff',
            autoLoad: false,
            model: 'DealMaker.model.Contact',
            proxy: {
                type: 'rest',
                limitParam: 'pagesize',
                noCache: false,
                startParam: 'offset',
                url: 'http://devnyespresso02/rest/default/hsjag/v1/mssql:Contact',
                reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.idContact){
                                    newdata.push(item);
                                }
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onBufferedStoreBeforeLoad,
                    scope: me
                }
            },
            sorters: {
                property: 'lastName'
            }
        }, cfg)]);
    },

    onBufferedStoreBeforeLoad: function(store, operation, eOpts) {
        var extraparams = {

        };
        if(store.sorters && store.sorters.getCount())
        {
            var sorter = store.sorters.getAt(0);
            extraparams.order = sorter._property+' '+sorter._direction;
            extraparams.sort = '';
        }
        if(store.filters && store.filters.getCount())
        {
            console.log(store.filters);
            var filters = store.filters;

            var filterstarr = [];

            for(var j=0;j<filters.getCount();j++){

                console.log(filters.getAt(j));
                var tempfilter = filters.getAt(j);

                if(tempfilter._operator==="like"){
                    var filterst = tempfilter._property + " " +tempfilter._operator + " " + "'%" +tempfilter._value+"%'";
                }else if(tempfilter._operator==="eq" || tempfilter._operator==="lt" || tempfilter._operator==="gt"){
                    if(tempfilter._operator==="eq"){
                        var opst = "=";
                    }
                    else if(tempfilter._operator==="lt"){
                        var opst = "<";
                    }
                    else if(tempfilter._operator==="gt"){
                        var opst = ">";
                    }
                    var filterst = tempfilter._property + " " +opst + " '"+tempfilter._value+"'";
                }
                filterstarr.push(filterst);
            }

            extraparams.filter = filterstarr;
        }else{
            extraparams.filter = [];
        }
        store.getProxy().extraParams = extraparams;
    },

    onProxyPrefetch: function(operation) {
        //console.log("On Proxy Prefetch");
        var me = this,
            resultSet = operation.getResultSet(),
            records = operation.getRecords(),
            successful = operation.wasSuccessful(),
            page = operation.getPage(),
            oldTotal = me.totalCount;



        if (operation.pageMapGeneration === me.getData().pageMapGeneration) {

            if (resultSet) {
                //console.log(records);
                //console.log(records.length);
                //console.log(me.totalCount);
                //console.log(operation.config.limit);
                var tempTotalCount;
                if(!me.totalCount){
                    tempTotalCount = operation.config.limit;
                }
                else{
                    tempTotalCount = me.totalCount;
                }
                if(records.length < operation.config.limit)
                {
                    me.totalCount = ((tempTotalCount + records.length) - operation.config.limit);
                    //me.totalCountFlag = 1;
                }else{
                    me.totalCount = tempTotalCount + (operation.config.limit);
                }
                //console.log(me.totalCount);
                //me.totalCount = resultSet.getTotal();
                if (me.totalCount !== oldTotal) {
                    me.fireEvent('totalcountchange', me.totalCount);
                }
            }


            if (page !== undefined) {
                delete me.pageRequests[page];
            }


            me.loading = false;
            me.fireEvent('prefetch', me, records, successful, operation);



            if (successful) {
                me.cachePage(records, operation.getPage());
            }


            Ext.callback(operation.getCallback(), operation.getScope() || me, [records, operation, successful]);
        }
    },

    getById: function(id) {
        var result = (this.snapshot || this.data).findBy(function(record) {
            return record.getId() === id;
        });
        if (this.buffered && !result) {
            return [];
        }
        return result;
    }

});