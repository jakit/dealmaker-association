/*
 * File: app/view/QuotesGridPanel.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.view.QuotesGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.quotesgridpanel',

    requires: [
        'DealMaker.view.QuotesGridPanelViewModel',
        'Ext.grid.column.Check',
        'Ext.grid.column.Number',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button',
        'Ext.menu.Menu'
    ],

    viewModel: {
        type: 'quotesgridpanel'
    },
    cls: 'gridcommoncls',
    store: 'DealQuotesAll',

    columns: [
        {
            xtype: 'checkcolumn',
            hidden: true,
            dataIndex: 'selected',
            text: 'Selected',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'bank',
            text: 'Bank',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'loantype',
            text: 'Loantype',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            width: 70,
            dataIndex: 'term',
            text: 'Term'
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'receivedamount',
            text: 'RxAmt',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'ratetype',
            text: 'Ratetype',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'spread',
            text: 'Spread',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'index',
            text: 'Index',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'indexvalue',
            text: 'Indexvalue',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'amortterm',
            text: 'Amortterm',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            width: 60,
            dataIndex: 'io',
            text: 'IO',
            flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            hidden: true,
            dataIndex: 'ppp',
            text: 'PPP',
            flex: 1
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            itemId: 'quotegridtoolbar',
            items: [
                {
                    xtype: 'button',
                    iconCls: 'add',
                    text: 'Add Quote',
                    menu: {
                        xtype: 'menu',
                        itemId: 'addquotetoolbarmenu'
                    }
                },
                {
                    xtype: 'button',
                    hidden: true,
                    itemId: 'receivequotedonebtn',
                    text: 'Done'
                }
            ]
        }
    ]

});