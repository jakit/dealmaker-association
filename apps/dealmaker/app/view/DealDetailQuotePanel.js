/*
 * File: app/view/DealDetailQuotePanel.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.view.DealDetailQuotePanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.dealdetailquotepanel',

    requires: [
        'DealMaker.view.DealDetailQuotePanelViewModel',
        'DealMaker.view.QuotesGridPanel',
        'Ext.grid.Panel',
        'Ext.form.field.Number',
        'Ext.button.Button',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.grid.column.Column',
        'Ext.view.Table',
        'Ext.toolbar.Toolbar',
        'Ext.grid.plugin.CellEditing',
        'Ext.form.FieldSet',
        'Ext.form.CheckboxGroup',
        'Ext.form.field.TextArea',
        'Ext.selection.CheckboxModel'
    ],

    viewModel: {
        type: 'dealdetailquotepanel'
    },
    frame: true,
    margin: 3,
    ui: 'activitypanel',
    layout: 'border',
    closable: true,

    items: [
        {
            xtype: 'quotesgridpanel',
            maxHeight: 225,
            collapsible: true,
            header: false,
            region: 'north',
            split: true
        },
        {
            xtype: 'panel',
            region: 'center',
            itemId: 'quoteeditorbtmpanel',
            layout: 'card',
            collapsible: true,
            header: false,
            items: [
                {
                    xtype: 'panel',
                    itemId: 'quotedetailspanel',
                    scrollable: true,
                    bodyPadding: 5,
                    header: {
                        titlePosition: 0,
                        items: [
                            {
                                xtype: 'button',
                                action: 'dealdetailquotesavebtn',
                                margin: '0 10 0 0',
                                itemid: 'dealdetailquotesavebtn',
                                text: 'Save'
                            },
                            {
                                xtype: 'button',
                                action: 'dealdetailquoteresetbtn',
                                itemid: 'dealdetailquoteresetbtn',
                                text: 'Reset'
                            },
                            {
                                xtype: 'button',
                                margin: '0 0 0 10',
                                action: 'dealdetailquoteclearbtn',
                                itemid: 'dealdetailquoteclearbtn',
                                text: 'Clear'
                            }
                        ]
                    },
                    title: 'Details',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            fieldLabel: 'idLoan',
                            name: 'loanid'
                        },
                        {
                            xtype: 'numberfield',
                            hidden: true,
                            fieldLabel: 'BankId',
                            name: 'bankid'
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Bank',
                                    labelWidth: 40,
                                    name: 'bank',
                                    readOnly: true
                                },
                                {
                                    xtype: 'button',
                                    itemId: 'quoteselectbankbtn',
                                    margin: '0 0 0 10',
                                    text: 'Select'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Loan Type',
                                    labelWidth: 80,
                                    name: 'loantype'
                                },
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Term',
                                    labelAlign: 'right',
                                    labelWidth: 50,
                                    name: 'term'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Rate Type',
                                    labelWidth: 65,
                                    name: 'ratetype'
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Rate',
                                    labelAlign: 'right',
                                    labelWidth: 50,
                                    name: 'rate'
                                },
                                {
                                    xtype: 'checkboxfield',
                                    flex: 1,
                                    fieldLabel: 'Spread',
                                    labelAlign: 'right',
                                    labelWidth: 80,
                                    name: 'spread'
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Index',
                                    labelAlign: 'right',
                                    labelWidth: 50,
                                    name: 'index'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Sent Amt.',
                                    labelWidth: 70,
                                    name: 'sentamount'
                                },
                                {
                                    xtype: 'datefield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Sent Date',
                                    labelAlign: 'right',
                                    labelWidth: 80,
                                    name: 'sentdate'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Recv. Amt.',
                                    labelWidth: 70,
                                    name: 'receivedamount'
                                },
                                {
                                    xtype: 'datefield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Recv. Date',
                                    labelAlign: 'right',
                                    labelWidth: 80,
                                    name: 'receiveddate'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Amort. Term',
                                    labelWidth: 78,
                                    name: 'amortterm'
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'IO',
                                    labelAlign: 'right',
                                    labelWidth: 60,
                                    name: 'io'
                                }
                            ]
                        },
                        {
                            xtype: 'gridpanel',
                            frame: true,
                            itemId: 'pppgridpanel',
                            margin: '5 0 5 0',
                            maxHeight: 200,
                            ui: 'activitypanel',
                            collapsible: true,
                            title: 'PPP',
                            store: 'PPP',
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'year',
                                    text: 'Year',
                                    flex: 1,
                                    editor: {
                                        xtype: 'textfield'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'ppptype',
                                    text: 'Type',
                                    flex: 1,
                                    editor: {
                                        xtype: 'textfield'
                                    }
                                },
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'rate',
                                    text: 'Rate',
                                    flex: 1,
                                    editor: {
                                        xtype: 'textfield'
                                    }
                                }
                            ],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    dock: 'top',
                                    items: [
                                        {
                                            xtype: 'button',
                                            itemId: 'add',
                                            iconCls: 'add',
                                            text: 'Add'
                                        }
                                    ]
                                }
                            ],
                            plugins: [
                                {
                                    ptype: 'cellediting'
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Initial Fee',
                                    labelWidth: 65,
                                    name: 'initialfee'
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Execution Type',
                                    labelAlign: 'right',
                                    name: 'exectype'
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            collapsible: true,
                            title: 'Options',
                            items: [
                                {
                                    xtype: 'checkboxgroup',
                                    fieldLabel: 'Options',
                                    hideLabel: true,
                                    labelWidth: 50,
                                    columns: 2,
                                    vertical: true,
                                    items: [
                                        {
                                            xtype: 'checkboxfield',
                                            boxLabel: ' Include Loan #'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            boxLabel: 'Include Client Name'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            boxLabel: 'Sent Via 1st Class Mail'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            boxLabel: 'Sent via Express Mail'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            boxLabel: 'Sent via Fax'
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            boxLabel: 'Sent via E-Mail'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            margin: '0 0 5 0',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Recv By',
                                    labelWidth: 60,
                                    name: 'initialfee'
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Via',
                                    labelAlign: 'right',
                                    labelWidth: 60,
                                    name: 'exectype'
                                }
                            ]
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Quoted By',
                            labelWidth: 70
                        },
                        {
                            xtype: 'fieldset',
                            collapsible: true,
                            items: [
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Bank Info'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Address'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Letter to'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Signed by'
                                },
                                {
                                    xtype: 'textareafield',
                                    anchor: '100%',
                                    fieldLabel: 'Special Note'
                                },
                                {
                                    xtype: 'textfield',
                                    anchor: '100%',
                                    fieldLabel: 'Received Note'
                                }
                            ]
                        },
                        {
                            xtype: 'gridpanel',
                            cls: 'gridcommoncls',
                            frame: true,
                            itemId: 'quotepropertygridpanel',
                            ui: 'activitypanel',
                            collapsible: true,
                            title: 'Properties',
                            columnLines: true,
                            multiColumnSort: true,
                            sortableColumns: false,
                            store: 'Dealproperty',
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        metaData.tdAttr = 'data-qtip="' + value + '"';
                                        return value;
                                    },
                                    width: 70,
                                    dataIndex: 'street_no',
                                    text: 'No.'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        metaData.tdAttr = 'data-qtip="' + value + '"';
                                        return value;
                                    },
                                    dataIndex: 'street',
                                    text: 'Street',
                                    flex: 1
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        metaData.tdAttr = 'data-qtip="' + value + '"';
                                        return value;
                                    },
                                    dataIndex: 'city',
                                    text: 'City',
                                    flex: 1
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        metaData.tdAttr = 'data-qtip="' + value + '"';
                                        return value;
                                    },
                                    width: 60,
                                    dataIndex: 'state',
                                    text: 'St.'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        metaData.tdAttr = 'data-qtip="' + value + '"';
                                        return value;
                                    },
                                    hidden: true,
                                    dataIndex: 'buildingClass',
                                    text: 'Bldg Cls'
                                },
                                {
                                    xtype: 'gridcolumn',
                                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                                        metaData.tdAttr = 'data-qtip="' + value + '"';
                                        return value;
                                    },
                                    hidden: true,
                                    dataIndex: 'propertyType',
                                    text: 'Prop Type'
                                }
                            ],
                            selModel: {
                                selType: 'checkboxmodel'
                            }
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            fieldLabel: 'Dealid',
                            name: 'dealid'
                        },
                        {
                            xtype: 'numberfield',
                            hidden: true,
                            fieldLabel: 'Activity Id',
                            name: 'activityid'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            fieldLabel: 'UsedInDeal',
                            name: 'usedInDeal'
                        }
                    ]
                }
            ]
        }
    ]

});