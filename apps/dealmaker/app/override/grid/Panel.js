Ext.define('DealMaker.override.grid.Panel', {
    override: 'Ext.grid.Panel'
    
});
Ext.define('DealMaker.override.dom.Element',{
	override: 'Ext.dom.Element',
  
	removeCls: function(names, prefix, suffix) {
        var SEPARATOR = '-',	// <------New
            spacesRe = /\s+/;	             // <------ New

		var me = this,
		    elementData = me.getData(),
		    hasNewCls, dom, map, classList, i, ln, name;
    	
		if (!names) {
		    return me;
		}
		
		if (!elementData) {    // <---------- New
		    return me;	   // <-----------New
		} 		   // <-----------New
		
		if (!elementData.isSynchronized) {
		    me.synchronize();
		} 
		
		dom = me.dom;
		map = elementData.classMap;
		classList = elementData.classList;
		prefix = prefix ? prefix + SEPARATOR : '';
		suffix = suffix ? SEPARATOR + suffix : '';
		if (typeof names === 'string') {
		    names = names.split(spacesRe);		
		}
		for (i = 0 , ln = names.length; i < ln; i++) {
		    name = names[i];
		    if (name) {
		        name = prefix + name + suffix;
		        if (map[name]) {
		            delete map[name];
		            Ext.Array.remove(classList, name);
		            hasNewCls = true;
		        }
		    }
		}
		if (hasNewCls) {
		    dom.className = classList.join(' ');
		}
		return me;
	}
});