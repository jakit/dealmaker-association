/*
 * File: app/controller/DealStatusHistoryController.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.controller.DealStatusHistoryController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({

        });
    },

    loadDealStatusHistory: function(dealid) {
        console.log("Load Deal Status History");
        var me = this;
        var deal_status_history_st = Ext.getStore('DealStatusHistory');
        deal_status_history_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        deal_status_history_st.load({
            params:{
                filter :'Deal_idDeal = '+dealid
            },
            callback: function(records, operation, success){
                /*var dealusergridtoolbar = me.getDealdetailusergridpanel().down('#dealusergridtoolbar');
                console.log("DealUser Cnt"+Ext.getStore('DealUsers').getCount());
                if(Ext.getStore('DealUsers').getCount()>0){
                    if(dealusergridtoolbar){
                        dealusergridtoolbar.hide();
                    }
                }else{
                    if(dealusergridtoolbar){
                        dealusergridtoolbar.show();
                    }
                }*/
            }
        });
    }

});
