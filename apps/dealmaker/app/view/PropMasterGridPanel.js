Ext.define('DealMaker.view.PropMasterGridPanel', {
    extend: 'DealMaker.view.component.grid.Dashboard',
    alias: 'widget.propmastergridpanel',
    requires: [ 'DealMaker.view.PropMasterGridPanelViewModel' ],
    viewModel: {
        type: 'propmastergridpanel'
    },
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        me.cls = 'gridcommoncls';
        me.itemId = 'propmastergridpanel';
        me.store = 'PropMasterallbuff';
        
        me.viewConfig = {
            forceFit: true
        };
        
        me.columns = [{
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'State',
            flex: .4,
            text: 'State'
        }, {
            xtype: 'gridcolumn',
            dataIndex: 'APN',
            flex: .5,
            text: 'APN'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'Boro',
            flex: .4,
            text: 'Boro'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'Block',
            flex: .4,
            text: 'Block'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
			hidden:true,
            dataIndex: 'Lot',
            flex: .4,
            text: 'Lot'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StreetNumber',
            flex: .4,
            text: 'Street No.'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'StreetName',
            flex: 1,
            text: 'Street'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'ZipCode',
            flex: .4,
            text: 'ZipCode'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Stories',
            flex: .4,
            text: 'Stories'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingClass',
            flex: .4,
            text: 'Building Class'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Units',
            flex: .4,
            text: 'Units'
        }, {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Owner',
            flex: 1,
            text: 'Owner'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            dataIndex: 'idPropertyMaster',
            flex: 1,
            text: 'Property No.'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            dataIndex: 'PropertyMasterCodeSource',
            flex: 1,
            text: 'PropertyMasterCodeSource'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Township',
            flex: 1,
            text: 'Township'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Municipality',
            flex: 1,
            text: 'Municipality'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingName',
            flex: 1,
            text: 'BuildingName'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'CurrentAddedValue',
            flex: 1,
            text: 'CurrentAddedValue'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'CurrentAVTAssess',
            flex: 1,
            text: 'CurrentAVTAssess'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'FullValue',
            flex: 1,
            text: 'FullValue'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Zoning',
            flex: 1,
            text: 'Zoning'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'FrontLot',
            flex: 1,
            text: 'FrontLot'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'LotDepth',
            flex: 1,
            text: 'LotDepth'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingFront',
            flex: 1,
            text: 'BuildingFront'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'BuildingDepartment',
            flex: 1,
            text: 'BuildingDepartment'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'LandArea',
            flex: 1,
            text: 'LandArea'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'Buildings',
            flex: 1,
            text: 'Buildings'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'ResidentialUnits',
            flex: 1,
            text: 'ResidentialUnits'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SquareFootage',
            flex: 1,
            text: 'SquareFootage'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'NumberOfBedrooms',
            flex: 1,
            text: 'NumberOfBedrooms'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'TotalAccumulatedLoanAmount',
            flex: 1,
            text: 'TotalAccumulatedLoanAmount'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'PropertyTax',
            flex: 1,
            text: 'PropertyTax'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SalePrice',
            flex: 1,
            text: 'SalePrice'
        }, {
            xtype: 'gridcolumn',
            hidden:true,
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'SaleDate',
            flex: 1,
            text: 'SaleDate'
        }];
        
        me.callParent(arguments);  
    }
});