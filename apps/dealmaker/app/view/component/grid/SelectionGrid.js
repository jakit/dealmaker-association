Ext.define('DealMaker.view.component.grid.SelectionGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.cmp-grid-selectiongrid',
    requires: [
        'Ext.grid.column.Column',
        'Ext.grid.column.Date',
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.XTemplate',
        'Filters.HeaderFields'        
    ],
    mixins: ['Filters.InjectHeaderFields', 'Filters.FiltersMixin'],
    scrollable: "y",
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;
        //me.header = false;
        me.cls = 'gridcommoncls';
        //me.frame = true;
        me.multiColumnSort = true;
        
        me.defaults = {
            headerField: [me.defaultField] // comes from FiltersMixin,js
        };
        var cfg = me.injectHeaderFields({
            useToolbar: false
            // hideable: true,
            // hidden: false,
            // position: "inner",
            // dockPosition: 'right',
            // buttons: me.filterButtons // comes from FiltersMixin.js
        });
        var headerFieldsFeature = Ext.create('Filters.HeaderFields', {cfg: cfg});
        me.features = [headerFieldsFeature];            
        me.callParent(arguments);  
   }    
});