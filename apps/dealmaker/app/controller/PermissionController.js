/*
 * File: app/controller/PermissionController.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.controller.PermissionController', {
    extend: 'Ext.app.Controller',

    refs: {
        permissionview: {
            autoCreate: true,
            selector: 'permissionview',
            xtype: 'permissionview'
        }
    },

    init: function(application) {
        this.control({
            'permissionview':{
                close: this.permissionViewClose
            },
            'permissionview #addpermission':{
                click: this.addPermissionBtnClick
            },
            '#permissionallgridpanel #savepermission':{
                click: this.savePermissionBtnClick
            }
        });
    },

    dealPermissionBtnClick: function() {
        var me = this;
        var eastregion = me.getController('DealDetailController').getDealdetailsformpanel().down('[region=east]');
        var permissionview = me.getPermissionview();
        eastregion.getLayout().setActiveItem(permissionview);

        var dealdetailform = me.getController('DealDetailController').getDealdetailsformpanel();
        var dealid = dealdetailform.dealrec.get('dealid');

        permissionview.setTitle("Permission > Deal > "+dealid);
        //sendloi.down('#activityid').setValue(activityid);

        var filterstarr = [];
        if(dealid!==''){
            var filterst = "objectid = '"+dealid+"'";
            filterstarr.push(filterst);
        }
        var filterst = "object = 'deal'";
        filterstarr.push(filterst);

        me.loadSelectedPermission(filterstarr);
    },

    permissionViewClose: function() {
        var me = this;
        var eastregion = me.getController('DealDetailController').getDealdetailsformpanel().down('[region=east]');
        eastregion.getLayout().setActiveItem(this.getController('DealDetailController').getDefaultworkareapanel());
    },

    loadSelectedPermission: function(filterstarr) {
        var me = this;
        var selpermission_st = Ext.getStore('SelectedPermission');
        selpermission_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        selpermission_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
            }
        });
    },

    loadAllPermission: function(filterstarr) {
        var me = this;
        var allpermission_st = Ext.getStore('AllPermission');
        allpermission_st.getProxy().setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+me.getController('MainController').getMainviewport().config.apikey+':1'
        });
        allpermission_st.load({
            scope: this,
            params:{
                filter :filterstarr
            },
            callback: function(records, operation, success) {
            }
        });
    },

    addPermissionBtnClick: function() {
        console.log("Add Permission");
        var me = this;
        if(!me.getPermissionview().down('permissionallgridpanel')){
            var perallgrid = Ext.create('DealMaker.view.PermissionAllGridPanel');
            me.getPermissionview().add(perallgrid);
        }
        me.getPermissionview().down('permissionallgridpanel').show();

        var filterstarr = [];

        //var filterst = "object = 'deal'";
        //filterstarr.push(filterst);

        me.loadAllPermission(filterstarr);
    },

    savePermissionBtnClick: function() {
        console.log("Save Permission");
        var me = this;
        var perallgrid = me.getPermissionview().down('#permissionallgridpanel');
        var selarr = perallgrid.getSelectionModel().getSelection();

        var dealdetailform = me.getController('DealDetailController').getDealdetailsformpanel();
        var dealid = dealdetailform.dealrec.get('dealid');

        if(selarr.length>0){
            var data = [];
            for(var i=0;i<selarr.length;i++){
                var perrec = {
                    objectType:1,
                    objectID:dealid,
                    //entityid:selarr[i].entityid,
                    entityname:selarr[i].entityname
                };
                data.push(perrec);
            }
            perallgrid.setLoading(true);
            Ext.Ajax.request({
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
                },
                url:DealMaker.view.AppConstants.apiurl+'mssql:ACL',
                params: Ext.util.JSON.encode(data),
                scope:this,
                success: function(response, opts) {
                    console.log("Save Permission is done.");
                    perallgrid.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    if(obj.statusCode == 201)
                    {
                        var filterstarr = [];
                        var filterst;
                        if(dealid!==''){
                            filterst = "objectid = '"+dealid+"'";
                            filterstarr.push(filterst);
                        }
                        filterst = "object = 'deal'";
                        filterstarr.push(filterst);
                        me.loadSelectedPermission(filterstarr);
                        perallgrid.hide();
                    }
                    else{
                        console.log("Add Contact to Bank Failure.");
                        Ext.Msg.alert('Contact add failed', obj.errorMessage);
                    }
                },
                failure: function(response, opts) {
                    console.log("Save Permission Failure.");
                    perallgrid.setLoading(false);
                    var obj = Ext.decode(response.responseText);
                    Ext.Msg.alert('Warning', obj.errorMessage);
                }
            });
        }else{
            Ext.Msg.alert("Warning","Please select permission.");
        }
    }

});
