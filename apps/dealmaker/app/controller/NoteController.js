Ext.define('DealMaker.controller.NoteController', {
    extend: 'Ext.app.Controller',

    refs: {
        dealnotesgridpanel: 'dealnotesgridpanel',
        noteoptionmenu: {
            autoCreate: true,
            selector: 'noteoptionmenu',
            xtype: 'noteoptionmenu'
        },
        notegridtoolbar: '#notegridtoolbar',
        dealdetailnotesgridpanel: 'dealdetailnotesgridpanel',
        dealdetailnotegridtoolbar: 'dealdetailnotesgridpanel #dealdetailnotegridtoolbar',
        notepanel: {
            autoCreate: true,
            selector: 'notepanel',
            xtype: 'notepanel'
        }
    },

    init: function(application) {
        this.control({
             'dealnotesgridpanel #addnotebtn': {
                 click: this.addNoteBtnClick
             },
            'dealnotesgridpanel': {
                 rowcontextmenu: this.notegridrightclick
             },
            'notepanel': {
                 close: this.notePanelClose
             },
            'notepanel #notesubmitbtn': {
                 click: this.submitNoteBtnClick
             },
            'notepanel #noteresetbtn': {
                 click: this.resetNoteBtnClick
             },
            'noteoptionmenu menuitem':{
                click: this.noteoptionitemclick
            },
            'dealdetailnotesgridpanel #addnotebtn': {
                 click: this.addNoteBtnClick
             },
            'dealdetailnotesgridpanel': {
                 rowcontextmenu: this.notegridrightclick
             }
        });
    },

    addNoteBtnClick: function(me) {
        console.log("Add Note Button Clicked.");
        var me = this;
		me.openNotePanel("add");
    },
	openNotePanel: function(mode){
		var me = this;
		var title = "";
		var dealnotesgrid;
        var dashboardview = this.getController('DashboardController').getDashboardview();
        var currViewId = dashboardview.getLayout().getActiveItem().getItemId();
		if(mode=="add"){
			title = "Add Note";
		}else if(mode=="edit"){
			title = "Edit Note";
			if(currViewId==="dealmenupanel"){
				dealnotesgrid = me.getDealnotesgridpanel();
			} else if(currViewId==="dealdetailsformpanel"){
				dealnotesgrid = me.getDealdetailnotesgridpanel();
			}
			if (dealnotesgrid.getSelectionModel().hasSelection()) {
				if(dealnotesgrid.getSelectionModel().getSelection().length == 1){
				}else{
					Ext.Msg.alert('Failed', "Please select only one note to edit.");
					return;
				}
			}
			else{
				Ext.Msg.alert('Failed', "Please select note to edit.");
				return;
			}
		}
		var notepanel;
        if(currViewId==="dealmenupanel"){
            me.getController('DocController').openDocViewerWindow();
			var docviewerwin =  me.getController('DocController').getDocviewerwindow();
			notepanel = me.getNotepanel();
			docviewerwin.add(notepanel);
			notepanel.getHeader().hide();			
			docviewerwin.setTitle(title);
        }
        else if(currViewId==="dealdetailsformpanel"){
            var eastregion = me.getController('DealDetailController').getDealdetailsformpanel().down('[region=east]');
			notepanel = me.getNotepanel();
            eastregion.getLayout().setActiveItem(notepanel);
			notepanel.getHeader().show();			
            notepanel.setTitle(title);
        }
		notepanel.getForm().findField('content').focus();
		if(mode=="add"){
			notepanel.getForm().reset();
		}else if(mode=="edit"){
			var row = dealnotesgrid.getSelectionModel().getSelection()[0];
			notepanel.getForm().loadRecord(row);
		}		
	},
    submitNoteBtnClick: function(btn) {
        console.log("Submit Note");
        var me = this;
        var dealgrid = this.getController('DealController').getDealmenugridpanel();
        if (dealgrid.getSelectionModel().hasSelection()) {
           var row = dealgrid.getSelectionModel().getSelection()[0];
           console.log(row.get('dealid'));
           var notefrm = me.getNotepanel();
		   var noteid = notefrm.getForm().findField('noteid').getValue();
		   console.log("noteid Value"+noteid);
		   var data,methodName,urlval;
		   if(noteid==0 || noteid==null){
			   data = [{
                	'content':notefrm.getForm().findField('content').getValue(),
					'Deal_idDeal': row.get('dealid')
			   }];
			   methodName = "POST";
			   urlval = DealMaker.view.AppConstants.apiurl+'Rnotes';
		   }else{
			   data = [{
					'@metadata': { 'checksum': 'override' },
					'idNote' :noteid,
					'content':notefrm.getForm().findField('content').getValue()
			   }];
			   methodName = "PUT";
			   urlval = DealMaker.view.AppConstants.apiurl+'mssql:Note/'+noteid;
		   }
           
           Ext.Ajax.request({
               headers: {
                   'Content-Type': 'application/json',
                   Accept: 'application/json',
                   Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
               },
               url:urlval,
			   method:methodName,
               params: Ext.util.JSON.encode(data),
               scope:this,
               success: function(response, opts) {
                   console.log("Note saved.");
                   var obj = Ext.decode(response.responseText);
                   if(obj.statusCode == 201 || obj.statusCode == 200)
                   {
                       me.loadNotesStore(row.get('dealid'));
                       notefrm.getForm().reset();
                       var dashboardview = me.getController('DashboardController').getDashboardview();
                       var currViewId = dashboardview.getLayout().getActiveItem().getItemId();
                       if(currViewId==="dealmenupanel"){
                            me.getController('DocController').closeDocViewerWindow();
                       }
                   }
                   else{
                       console.log("Note Submittion Failure.");
                       Ext.Msg.alert('Note Save failed', obj.errorMessage);
                   }
               },
               failure: function(response, opts) {
                   console.log("Note Submittion Failure.");
                   var obj = Ext.decode(response.responseText);
                   Ext.Msg.alert('Note Save failed', obj.errorMessage);
               }
           });
        }
        else{
            Ext.Msg.alert('Failed', "Please select deal from dealgrid.");
        }
    },

    resetNoteBtnClick: function(me) {
        console.log("Reset Add Note Form");
        this.getNotepanel().getForm().reset();
    },

    loadNotesStore: function(dealid) {
        console.log("Notes Load");
        var me = this;
        var notesproxy = Ext.getStore('Notes').getProxy();
        notesproxy.setHeaders({
            Accept: 'application/json',
            Authorization: 'Espresso '+this.getController('MainController').getMainviewport().config.apikey+':1'
        });
        Ext.getStore('Notes').load({
            params:{
                filter :'dealid = '+dealid
            },
            callback: function(records, operation, success){
                var notetoolbar = me.getNotegridtoolbar();
                var detailnotetoolbar = me.getDealdetailnotegridtoolbar();
                console.log("Note Cnt"+Ext.getStore('Notes').getCount());
                if(Ext.getStore('Notes').getCount()>0){
                    if(notetoolbar){
                        notetoolbar.hide();
                    }
                    if(detailnotetoolbar){
                        detailnotetoolbar.hide();
                    }
                }else{
                    if(notetoolbar){
                        notetoolbar.show();
                    }
                    if(detailnotetoolbar){
                        detailnotetoolbar.show();
                    }
                }
            }
        });
    },
	
    notegridrightclick: function(me, record, tr, rowIndex, e, eOpts) {
        e.stopEvent();
        console.log("Show Menu for Note Edit/Histroy");
        if(this.getNoteoptionmenu()){
            this.getNoteoptionmenu().showAt(e.getXY());
        }
    },

    noteoptionitemclick: function(item, e, eOpts) {
        console.log("Note Menu Item Clicked.");
        if(item.getItemId()==="noteedit"){
            console.log("Note Edit Menu Item Clicked");
            this.openNotePanel("edit");
        }else if(item.getItemId()==="notehistory"){
            console.log("Note History Menu Item Clicked");
        }else if(item.getItemId()==="noteadd"){
            console.log("Note Add Menu Item Clicked");
			this.openNotePanel("add");
        }
    },

    notePanelClose: function() {
        var eastregion = this.getController('DealDetailController').getDealdetailsformpanel().down('[region=east]');
        eastregion.getLayout().setActiveItem(this.getController('DealDetailController').getDefaultworkareapanel());
    }

});
