Ext.define('DealMaker.view.DealMenuLoanGridPanel', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.dealmenuloangridpanel',

    requires: [
        'DealMaker.view.DealMenuLoanGridPanelViewModel',
        'Ext.view.Table',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date'
    ],

    viewModel: {
        type: 'dealmenuloangridpanel'
    },
    cls: 'dealmenuloangridcls',
    frame: true,
    ui: 'propertyloansubgrid',
    header: false,
    store: 'Loansdeal',
	margin:'0 0 0 5',
    columns: [
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'bank',
            text: 'Bank',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'loanamt',
            text: 'LoanAmt.',
            format: '0,000',
			flex: 1
        },
        {
            xtype: 'datecolumn',
            dataIndex: 'loandate',
            text: 'Loan Date',
			flex: 1
        },
        {
            xtype: 'gridcolumn',
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },
            dataIndex: 'loantype',
            text: 'Loan Type',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'loanrate',
            text: 'Loan Rate',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'loanspread',
            text: 'Loan Spread',
			flex: 1
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'term',
            text: 'Term',
            flex: 1
        }
    ],

    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            store : me.buildStore()
        });

        me.callParent(arguments);
    },

    buildStore: function() {
        return Ext.create('DealMaker.store.Loansdeal', {});
    },
	listeners:{
		'itemmouseenter': function( view, record, item, index, e, eOpts ){
			console.log("Hover on Loan Rec : "+record.get('bank'));
			
			var dealmenuprloanpanel = view.grid.ownerCt;
			var grids = dealmenuprloanpanel.query('grid');
			//console.log(grids);
			var prptygrid = grids[0];
			var loangrid  = grids[1];
			
			var loanid = record.get('loanid');
			
			var properties_st = Ext.getStore('Properties');
			properties_st.each(function(prloanrec){
				if(prloanrec.get('loanid')===loanid){
					var propertyid = prloanrec.get('propertyid');
					var propertyrec = prptygrid.getStore().getById(propertyid);
					var rowIndex = prptygrid.store.indexOf(propertyrec);
					//console.log("rowIndex"+rowIndex);
					//Fine Row Index from grid using rec
					prptygrid.getView().addRowCls(rowIndex, 'hover-row-style');
				}
			},this);
		},
		'itemmouseleave': function( view, record, item, index, e, eOpts ){
			console.log("Hover on Loan Rec : "+record.get('bank'));
			
			var dealmenuprloanpanel = view.grid.ownerCt;
			var grids = dealmenuprloanpanel.query('grid');
			//console.log(grids);
			var prptygrid = grids[0];
			var loangrid  = grids[1];
			
			var loanid = record.get('loanid');
			
			var properties_st = Ext.getStore('Properties');
			properties_st.each(function(prloanrec){
				if(prloanrec.get('loanid')===loanid){
					var propertyid = prloanrec.get('propertyid');
					var propertyrec = prptygrid.getStore().getById(propertyid);
					var rowIndex = prptygrid.store.indexOf(propertyrec);
					//Fine Row Index from grid using rec
					prptygrid.getView().removeRowCls(rowIndex, 'hover-row-style');
				}
			},this);
		}
	}

});