/*
 * a list layout and a detail layout
 * me.listLayout ref as itemId: 'listLayout'
 * me.detailLayout ref as itemId: 'detailLayout'
 * layoutFocus can be set as 'listLayout or 0' or 'detailLayout or 1'
 * switchPanel(index) // 0 or 1
 * setData(data) // this will set the data property
 * getData() // this will retrieve the data property
 * 
 */

Ext.define('ListToDetail.view.component.container.ListToDetail', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.cmp-container-listtodetail',
    requires: [
        'Ext.layout.container.Card'       
    ],
    constructor: function(cfg) {
        var me = this;
        Ext.apply(me, cfg);
        me.callParent(arguments);
    },
    initComponent: function() {
        var me = this;

        me.layout = {
            type: 'card'
        };
        
        me.deferredRender = false;

        var listLayout = {
            itemId: 'listLayout'
        };
        var detailLayout = {
            itemId: 'detailLayout',
            closable: true,
            closeAction: 'hide',
            onHide: Ext.bind(function() {
                var me = this;
                me.switchPanel(0);
            }, me)               
        };
        
        Ext.apply(listLayout, me.listLayout);
        Ext.apply(detailLayout, me.detailLayout);
        
        me.items = [listLayout, detailLayout];
        me.activeItem = 1;
        if(me.layoutFocus == "listLayout" || me.layoutFocus == 0) {
            me.activeItem = 0;
        } 
        
        me.callParent(arguments);  
   },
   switchPanel: function(panelIdx) {
       var me = this;
       me.getLayout().setActiveItem(panelIdx);
   },
   listLayout: null,
   detailLayout: null,
   layoutFocus: 'listLayout',
   data: null,
   setData: function(data) {
        var me = this;
        me.data = data;        
   },
   getData: function() {
        var me = this;
        return me.data;
   }
    
});