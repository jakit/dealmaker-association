Ext.define('DealMaker.model.Loandeal', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],
	idProperty : 'loanid',
    fields: [
        {
            name: 'dealid'
        },
        {
            name: 'street_no'
        },
        {
            name: 'street'
        },
        {
            name: 'city'
        },
        {
            name: 'state'
        },
        {
            name: 'buildingClass'
        },
        {
            name: 'propertyType'
        },
        {
            name: 'bank'
        },
		{
            name: 'loanid'
        },
        {
            name: 'loanamt'
        },
        {
            name: 'loandate'
        },
        {
            name: 'loantype'
        },
        {
            name: 'loanrate'
        },
        {
            name: 'loanspread'
        },
        {
            name: 'term'
        },
        {
            name: 'idDeal_has_Property'
        },
        {
            name: 'propertyid'
        },
        {
            name: 'estimatedloansize'
        },
        {
            name: 'income'
        },
        {
            name: 'expenses'
        },
        {
            name: 'sqfr'
        },
        {
            name: 'primaryProperty'
        },
        {
            name: 'zipCode'
        },
        {
            name: 'APN'
        },
        {
            name: 'block'
        },
        {
            name: 'Lot'
        },
        {
            name: 'Boro'
        }
    ]
});