/*
 * File: app/model/Deal.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.model.Deal', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    idProperty: 'dealid',

    fields: [
        {
            name: 'dealid'
        },
        {
            name: 'name'
        },
        {
            name: 'street_no'
        },
        {
            name: 'street'
        },
        {
            name: 'city'
        },
        {
            name: 'state'
        },
        {
            name: 'maincontact'
        },
        {
            name: 'status'
        },
        {
            name: 'startdate'
        },
        {
            name: 'bank'
        },
        {
            name: 'loanamt'
        },
        {
            name: 'loandate'
        },
        {
            convert: function(v, rec) {
                if(rec.get('loandate'))
                {
                    var loandtarr = rec.get('loandate').split("T");
                    return loandtarr[0];
                }
            },
            name: 'loandisplaydate'
        },
        {
            name: 'broker'
        },
        {
            name: 'dealtype'
        },
        {
            convert: function(v, rec) {
                if(rec.get('dealdate'))
                {
                    var dealdtarr = rec.get('dealdate').split("T");
                    return dealdtarr[0];
                }
            },
            name: 'dealdisplaydate'
        },
        {
            name: 'allcontacts'
        },
        {
            name: 'allPropertyNames'
        }
    ]
});