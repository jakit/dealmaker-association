Ext.define('DealMaker.store.Propertiesdeal', {
    extend: 'Ext.data.Store',

    requires: [
        'DealMaker.model.Propertydeal',
        'Ext.data.proxy.Rest'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'Propertiesdeal',
            model: 'DealMaker.model.Propertydeal',
            proxy: {
                type: 'rest',
                noCache: false,
                url: 'http://devnyespresso02/rest/default/hsjag/v1/mssql:v_propertyAndLoanPerDeal',
				//url: 'http://localhost:82/mc-workspacev1/data/v_propertyAndLoanPerDeal.json',
				reader: {
                    type: 'json',
                    transform: {
                        fn: function(data) {
                            // do some manipulation of the raw data object
                            var newdata = [];
                            Ext.Array.each(data, function(item, index, countriesItSelf) {
                                if(item.propertyid!=null && item.propertyid!=0){
                                    newdata.push(item);
                                }
                            });
                            return newdata;
                        },
                        scope: this
                    }
                }
            }
        }, cfg)]);
    }
});