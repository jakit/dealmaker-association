Ext.define('DealMaker.view.override.DealDetailDocsGridPanel', {
    override: 'DealMaker.view.DealDetailDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});