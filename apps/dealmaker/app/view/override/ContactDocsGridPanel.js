Ext.define('DealMaker.view.override.ContactDocsGridPanel', {
    override: 'DealMaker.view.ContactDocsGridPanel',
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});