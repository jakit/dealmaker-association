Ext.define('DealMaker.view.override.BankDocsGridPanel', {
    override: 'DealMaker.view.BankDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});