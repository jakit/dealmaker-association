Ext.define('DealMaker.view.override.DealDocsSendLOIGridPanel', {
    override: 'DealMaker.view.DealDocsSendLOIGridPanel',

    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});