function attachEvents(obj)
{
    console.log("Attach Events Called");
    //console.log(obj);
    // Attach event handlers for Click & KeyPress events.
    addEvent(obj, 'FilesLoaded', handleDrop);
}
function addEvent(obj, name, func)
{
    //console.log("addEvent"+name);    
    if(obj.attachEvent)
    { obj.attachEvent('on'+name, func); }
    else if(obj.addEventListener)
    { obj.addEventListener(name, func, false); }
    else { alert("failed to attach event"); }
}

function handleDrop(fna, fca)
{
    console.log("handleDrop");
    var dealactivitycntrl = DealMaker.app.getController('DealActivityController');
    dealactivitycntrl.getGlobalDocFromOutlook(fna, fca);
}
//// Submission Outlook Doc Events
function attachSubmissionOutlookEvents(obj)
{
    console.log("Attach Submission Events Called");
    //console.log(obj);
    // Attach event handlers for Click & KeyPress events.
    addEvent(obj, 'FilesLoaded', handleSubmOutlookDrop);
}
function handleSubmOutlookDrop(fna, fca)
{
    console.log("handleSubmOutlookDrop");
    var dealactivitycntrl = DealMaker.app.getController('DealActivityController');
    dealactivitycntrl.getSubmissionDocFromOutlook(fna, fca);
}
//// General Outlook Doc Events /////
function attachGeneralOutlookEvents(obj){
    console.log("Attach General Events Called");
    //console.log(obj);
    // Attach event handlers for Click & KeyPress events.
    addEvent(obj, 'FilesLoaded', handleGeneralOutlookDrop);
}
function handleGeneralOutlookDrop(fna, fca)
{
    console.log("handleGeneralOutlookDrop");
    var generaldocselcntrl = DealMaker.app.getController('GeneralDocSelController');
    generaldocselcntrl.getGeneralDocFromOutlook(fna, fca);
}
//// Deal Outlook Doc Events /////
function attachDealDocOutlookEvents(obj){
    console.log("Attach DealDoc Events Called");
    //console.log(obj);
    // Attach event handlers for Click & KeyPress events.
    addEvent(obj, 'FilesLoaded', handleDealDocOutlookDrop);
}
function handleDealDocOutlookDrop(fna, fca)
{
    console.log("handleDealDocOutlookDrop");
    var doccntrl = DealMaker.app.getController('DocController');
    doccntrl.getDealDocFromOutlook(fna, fca);
}
//// Deal Detail Outlook Doc Events /////
function attachDealDetailDocOutlookEvents(obj){
    console.log("Attach DealDetailDoc Events Called");
    //console.log(obj);
    // Attach event handlers for Click & KeyPress events.
    addEvent(obj, 'FilesLoaded', handleDealDetailDocOutlookDrop);
}
function handleDealDetailDocOutlookDrop(fna, fca)
{
    console.log("handleDealDetailDocOutlookDrop");
    var doccntrl = DealMaker.app.getController('DocController');
    doccntrl.getDealDetailDocFromOutlook(fna, fca);
}