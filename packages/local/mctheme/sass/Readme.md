# mctheme/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    mctheme/sass/etc
    mctheme/sass/src
    mctheme/sass/var
