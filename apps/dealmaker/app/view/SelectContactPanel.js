Ext.define('DealMaker.view.SelectContactPanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.selectcontactpanel',

    requires: [
        'DealMaker.view.SelectContactPanelModel',
        'Ext.grid.Panel',
        'Ext.grid.column.Column',
        'Ext.grid.filters.filter.String',
        'Ext.view.Table',
        'Ext.grid.filters.Filters',
        'Ext.button.Button',
        'Ext.form.field.ComboBox',
		'DealMaker.view.component.grid.SelectionGrid'
    ],

    viewModel: {
        type: 'selectcontactpanel'
    },
    frame: true,
    itemId: 'selectcontactpanel',
    margin: 3,
    ui: 'activitypanel',
    layout: 'border',
    closable: true,
    title: 'Add Contact',

    items: [
        {
            xtype: 'cmp-grid-selectiongrid',
			header: false,
            region: 'center',
            itemId: 'selectcontactgridpanel',
            scrollable: true,
            store: 'UwContactAllSelBuff',
            columns: [
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    items: [
                        {
                            xtype: 'searchtrigger',
                            autoSearch: true,
                            tabIndex: 1
                        }
                    ],
                    dataIndex: 'firstName',
                    text: 'FirstName',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    items: [
                        {
                            xtype: 'searchtrigger',
                            autoSearch: true,
                            tabIndex: 2
                        }
                    ],
                    dataIndex: 'lastName',
                    text: 'LastName',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    items: [
                        {
                            xtype: 'searchtrigger',
                            autoSearch: true,
                            itemId: 'companyfield',
                            tabIndex: 3
                        }
                    ],
                    dataIndex: 'companyName',
                    text: 'Company',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },                   
                    dataIndex: 'title',
                    text: 'Title',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                },
				{
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'contactType',
                    text: 'CntType',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    hidden: true,
                    dataIndex: 'officePhone_1',
                    text: 'Phone',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    hidden: true,
                    dataIndex: 'email_1',
                    text: 'Email',
                    flex: 1,
                    filter: {
                        type: 'string'
                    }
                }
            ],
            plugins: [
                {
                    ptype: 'gridfilters'
                }
            ]
        },
        {
            xtype: 'panel',
            split: true,
	        collapsible: true,
            region: 'south',
            scrollable: true,
            layout: 'column',
            bodyPadding: 5,
            header: {
                titlePosition: 0,
                items: [
                    {
                        xtype: 'button',
                        margin: '0 10 0 0',
                        action: 'saveContactToExecBtn',
                        itemId: 'saveContactToExecBtn',
                        text: 'Save'
                    }
                ]
            },
            title: 'Details',
            items: [
                {
                    xtype: 'panel',
                    columnWidth: 0.5,
                    margin: '0 5 0 0',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Name',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'fullName',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Company',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'companyName',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Title',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'title',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Phone 1',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'officePhone_1',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Phone 2',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'officePhone_2',
                            readOnly: true
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.5,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            ui: 'dealinfofield',
                            fieldLabel: 'Role',
                            labelAlign: 'right',
                            labelWidth: 70,
                            name: 'contactType',
                            selectOnFocus: true,
                            displayField: 'type',
                            store: 'ContactTypes',
                            valueField: 'type'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Address',
                            labelAlign: 'right',
                            labelWidth: 70,
                            name: 'address',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'zipCode',
                            labelAlign: 'right',
                            labelWidth: 70,
                            name: 'zipCode',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Email 1',
                            labelAlign: 'right',
                            labelWidth: 70,
                            name: 'email_1',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Email 2',
                            labelAlign: 'right',
                            labelWidth: 70,
                            name: 'email_2',
                            readOnly: true
                        }
                    ]
                }
            ]
        }
    ]

});