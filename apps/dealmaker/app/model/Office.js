Ext.define('DealMaker.model.Office', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.field.Field'
    ],

    fields: [
        {
            name: 'idOffice'
        },
        {
            name: 'description'
        }
    ]
});