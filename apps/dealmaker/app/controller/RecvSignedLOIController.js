/*
 * File: app/controller/RecvSignedLOIController.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.controller.RecvSignedLOIController', {
    extend: 'Ext.app.Controller',

    init: function(application) {
        this.control({

        });
    },

    RecvSignLOIClick: function(activityid, activitytext, h_fname) {
        var me = this;

        me.getController('GeneralDocSelController').showGeneralDocSelPanel('RecvSignLOI');
    },

    closeRecvSignedLOI: function() {
        console.log("Close Recv Sign LOI");
        var eastregion = this.getController('DealDetailController').getDealdetailsformpanel().down('[region=east]');
        eastregion.getLayout().setActiveItem(this.getController('DealDetailController').getDefaultworkareapanel());
    }

});
