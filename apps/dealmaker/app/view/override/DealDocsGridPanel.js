Ext.define('DealMaker.view.override.DealDocsGridPanel', {
    override: 'DealMaker.view.DealDocsGridPanel',
    
    initComponent:function(){            
        this.plugins = [
            Ext.create('Ext.grid.plugin.FileDrop', {
                readType: 'DataURL'
            })
        ];
        this.callOverridden(arguments);
    }
});