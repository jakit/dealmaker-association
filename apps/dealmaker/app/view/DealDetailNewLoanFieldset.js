/*
 * File: app/view/DealDetailNewLoanFieldset.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.1.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.1.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('DealMaker.view.DealDetailNewLoanFieldset', {
    extend: 'Ext.form.Panel',
    alias: 'widget.dealdetailnewloanfieldset',

    requires: [
        'DealMaker.view.DealDetailNewLoanFieldsetViewModel',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.view.Table',
        'Ext.panel.Tool'
    ],

    viewModel: {
        type: 'dealdetailnewloanfieldset'
    },
    frame: true,
    margin: '0 0 0 5',
    ui: 'newloandealpanel',
    layout: 'fit',
    collapsible: false,
    header: {
        titlePosition: 1,
        items: [
            {
                xtype: 'image',
                margin: '0 10 0 0',
                width: '24px',
                height: '24px',
                src: 'resources/images/icons/loan_m.png'
            }
        ]
    },
    title: 'NEW LOAN',

    items: [
        {
            xtype: 'panel',
            formel: 'yes',
            padding: '5 0 0 0',
            scrollable: true,
            layout: 'column',
            items: [
                {
                    xtype: 'panel',
                    columnWidth: 0.45,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Bank',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'bank'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'RxAmt',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'receivedamount'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'PPP',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'ppp'
                        },
                        {
                            xtype: 'panel',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Option',
                                    labelAlign: 'right',
                                    labelWidth: 50
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    ui: 'dealinfofield',
                                    fieldLabel: 'Amort',
                                    labelAlign: 'right',
                                    labelWidth: 50,
                                    name: 'amortterm'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.25,
                    margin: '0px 5px 0px 0px',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'IO',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'io'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Index',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'index'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Spread',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'spread'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Term',
                            labelAlign: 'right',
                            labelWidth: 50,
                            name: 'term'
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    columnWidth: 0.3,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Ratetype',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'ratetype'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Loantype',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'loantype'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Indexval',
                            labelAlign: 'right',
                            labelWidth: 65,
                            name: 'indexvalue'
                        },
                        {
                            xtype: 'textfield',
                            ui: 'dealinfofield',
                            fieldLabel: 'Mtg Type',
                            labelAlign: 'right',
                            labelWidth: 65
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'gridpanel',
            cls: 'gridcommoncls',
            hidden: true,
            itemId: 'dealdetailnewloangridpanel',
            columnLines: true,
            multiColumnSort: true,
            sortableColumns: false,
            store: 'Quotes',
            columns: [
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'bank',
                    text: 'Bank',
                    flex: 1
                },
                {
                    xtype: 'numbercolumn',
                    dataIndex: 'receivedamount',
                    text: 'RxAmt',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'ratetype',
                    text: 'Ratetype',
                    flex: 1
                },
				{
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'loantype',
                    text: 'LoanType',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'term',
                    text: 'Term',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'amortterm',
                    text: 'Amort',
                    flex: 1
                },
                {
                    xtype: 'gridcolumn',
                    renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        return value;
                    },
                    dataIndex: 'ppp',
                    text: 'PPP',
                    flex: 1
                }
            ]
        }
    ],
    tools: [
        {
            xtype: 'tool',
            itemId: 'newloandetailclosebtn',
            type: 'close'
        },
        {
            xtype: 'tool',
            hidden: true,
            itemId: 'adddealloanbtn',
            type: 'plus'
        }
    ]

});