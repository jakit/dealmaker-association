Ext.define('Filters.FiltersMixin', {
    // filterButtons: [{
        // xtype: 'button',
        // iconCls: "filters-toolbar-apply-filter",
        // tooltip: "Apply Filters",
        // listeners: {
            // "click": {
                // fn: function(btn) {
                    // btn.up('grid').applyFilters();
                // }
            // }
        // }
    // }],    
    
    filterButtons: [{
        xtype: 'button',
        iconCls: "filters-toolbar-clear-filter",
        tooltip: "Clear Filters",
        listeners: {
            "click": {
                fn: function(btn) {
                    var grid = btn.up('grid');
                    btn.up('grid').clearFilters.apply(grid, [false]);
                }
            }
        }
    }],
    
    defaultField: {
        xtype: 'textfield',
        enableKeyEvents: true,
        margin: "0",
        listeners: {
            change: {
                fn: Ext.Function.createBuffered(function(field) {field.up('grid').applyFilters();}, 250, this)
            }
        }
    },
    applyFilters: function() {
        var me = this;
        var fn = function() {
            var me = this;
            var fields = me.HFAPI().getAllFields();
            var x, l = fields.length, field;
            var val;
            var filters = [];
            for(x = 0; x < l; x++) {
                field = fields[x];
                if(field) {
                    val = Ext.String.trim(field.getValue());
                    if(val.length > 0) {
                        var newObj = {
                            id: field.up().dataIndex,
                            property: field.up().dataIndex,
                            value: val,
                            exactMatch: false,
                            caseSensitive: false
                        };
                        newObj.operator = "like";
                        filters.push(newObj);
                    }
                }
            }
            if(filters.length > 0) {
                me.getStore().addFilter(filters);    
            } else {
                me.clearFilters(true);
            }
        };
        var fn2 = Ext.Function.bind(fn, me);
        window.setTimeout(fn2, 50);
    },
    clearFilters: function(doit) {
        var me = this;
        var fn = function() {
            var me = this;
            var fields = me.HFAPI().getAllFields();
            var x, l = fields.length, field;
            var val;
            var filters = [];
            var removeAtKey = [];
            var doClearFilter = doit;
            for(x = 0; x < l; x++) {
                field = fields[x];
                if(field) {
                    val = Ext.String.trim(field.getValue());
                    if(val.length > 0) {
                        doClearFilter = true;
                        removeAtKey.push(field.up().dataIndex);
                        field.suspendEvents();
                        field.setValue(null);
                        field.resumeEvents({
                            discardQueue: true
                        });
                    }
                }
            }
           if(doClearFilter) {
                me.getStore().clearFilter();               
           }
        };
        var fn2 = Ext.Function.bind(fn, me);
        window.setTimeout(fn2, 50);
    }    
});
